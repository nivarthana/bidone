import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public _pageSize: number;
  public _baseUri: string;
  public _customSuffix;
  constructor(public http: HttpClient) {

  }

  set(baseUri: string, pageSize?: number, customSuffix?: string): void {
    this._baseUri = baseUri;
    this._pageSize = pageSize;
    this._customSuffix = customSuffix;
  }

  post(data?: any, mapJson: boolean = true) {
    if (mapJson)
      return this.http.post(this._baseUri, data, this.getRequestOptions()).
        pipe(map(response => <any>(<Response>response).json()));
    else
      console.log(data);
    return this.http.post(this._baseUri, data, this.getRequestOptions());
  }


  private getRequestOptions(data?: any) {
    let options = { headers: this.getHeaders() };
    return options;
  }

  private getHeaders() {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return headers;
  }
}
