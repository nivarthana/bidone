﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BidOne.DTO;
using BidOne.ServiceLayer;
using Microsoft.AspNetCore.Mvc;

namespace BidOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok();
        }

       
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return Ok();
        }

        
        [HttpPost]
        public ActionResult Post([FromBody] SimpleModel value)
        {      
            JSONPersistence.writeJSONToFile(value);
            return Ok();
        }

       
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] SimpleModel value)
        {
            throw new NotImplementedException();
        }

      
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
