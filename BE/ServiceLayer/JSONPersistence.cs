﻿using BidOne.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BidOne.ServiceLayer
{
    public static class JSONPersistence
    {
            
        public static void writeJSONToFile(SimpleModel data)
        {
            try
            {
                using (StreamWriter file = new StreamWriter(@"./path.txt", true))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    //serialize object directly into file stream
                    serializer.Serialize(file, data);
                }
            }
            catch(IOException ex)
            {
                //this is redundant - we can just let exceptions bubble up since we are't doing any meaning full error handlign here
                throw;
            }
      

        }
        
    } 
}
