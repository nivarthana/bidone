1. Go inside BidOne\BE folder and run the backend



2. Go inside FE - project based on Angular 6 and Angular-CLI, 
would need to have compatibale node and npm versions installed.
Latest stable major branch of npm and node should work.


> npm i 
> npm run start 

3. Make sure FE is  pointing to the correct BE url (should be by default - hard coded to http://localhost:4255 for this simple test page)
   If not change it in app.compenent


============================================================

In an enterprise app you would have an API gateway with auth checks - microservices behind the gateway possibly with a queue system - 
SPA to app communication over https - and much more error handling built in to it. I've left this baggage from this test page.