import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { DataService } from './services/dataservice.service';
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormGroup } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { NotifierService } from "angular-notifier";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  simpleForm: FormGroup;

  public config: ToasterConfig =  new ToasterConfig({
                              showCloseButton: true,
                              tapToDismiss: false,
                              timeout: 0
                            });
  constructor(private fb: FormBuilder,
    private dataService: DataService,
    private toasterService: ToasterService) {
    this.createForm();
  }

  createForm() {
    this.simpleForm = this.fb.group({
      firstname: '',
      lastname: ''
    });
  }


  onSubmit() {
    const formModel = this.simpleForm.value;
    const postModel = JSON.stringify(formModel);
    this.dataService.set('http://localhost:4255/api/test');
    this.dataService.post(postModel, false).subscribe();
    this.toasterService.pop('success','', 'data posted on server!')
    this.rebuildForm();
  }

  revert() { this.rebuildForm(); }

  rebuildForm() {
    this.simpleForm.reset({
      firstname: '',
      lastname: ''
    });

  }

}
